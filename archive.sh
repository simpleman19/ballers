#!/bin/bash

TAG=$(git describe --tags)

rm ballers.tgz

mkdir /tmp/ballers

cp -r * /tmp/ballers

tar --exclude=hashed.txt --exclude=.git --exclude=*.pyc --exclude=__pycache__ --exclude=.idea --exclude=ballers.tgz --exclude=.env --exclude=database/app.db -zcvf "ballers_${TAG}.tgz" -C /tmp/ballers/ .

rm -rf /tmp/ballers
