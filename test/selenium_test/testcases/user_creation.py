from time import sleep
from test.selenium_test.framework import SeleniumBaseTest


class TestUserLogin(SeleniumBaseTest):

    def test_navigation(self):
        self.assertTrue("Login" in self.driver.title, msg="Page is not a login page")
        d = self.driver
        d.find_element_by_id("signUpButton").click()
        self.assertTrue("Sign" in self.driver.title, msg="Failed to redirect to login page")

    def test_user_creation(self):
        d = self.driver
        d.find_element_by_id("signUpButton").click()
        self.fill_in_signup_form()
        d.find_element_by_id('submit-button').click()
        sleep(.5)
        self.assertTrue("Home Page" in d.title, "Failed to sign up for an account")

    def test_user_creation2(self):
        d = self.driver
        d.find_element_by_id("signUpButton").click()
        self.fill_in_signup_form()
        d.find_element_by_id('submit-button').click()
        sleep(.5)
        self.assertTrue("Home Page" in d.title, "Failed to sign up for an account")

    def test_logout(self):
        d = self.driver
        self.test_user_creation()
        d.find_element_by_id("main-logout-button").click()
        self.assertTrue("Login" in d.title, "Failed to log out")

    def test_login(self):
        self.test_logout()
        d = self.driver
        d.find_element_by_id("email").send_keys("testing@test.com")
        d.find_element_by_id("password").send_keys("testing")
        d.find_element_by_id('submit-button').click()
        sleep(.5)
        self.assertTrue("Home Page" in d.title, "Failed to log out")

    def fill_in_signup_form(self):
        self.driver.find_element_by_id("email").send_keys("testing@test.com")
        self.driver.find_element_by_id("password").send_keys("testing")
        self.driver.find_element_by_id("confirm").send_keys("testing")
        self.driver.find_element_by_id("first_name").send_keys("John")
        self.driver.find_element_by_id("last_name").send_keys("Doe")
