<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{% block title %}Default title{% endblock title %}</title>
    <meta name="description" content="{% block metadescription %}{% endblock metadescription %}">
    <meta name="keywords" content="{% block metakeywords %}{% endblock metakeywords %}">
    <script type="text/javascript" src="/static/materialize/js/materialize.min.js"></script>

    <!--Import Google Icon Font
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <style>
        @font-face {
            font-family: 'Material Icons';
            font-style: normal;
            font-weight: 400;
            src: url(/static/iconfont/MaterialIcons-Regular.eot); /* For IE6-8 */
            src: local('Material Icons'),
            local('MaterialIcons-Regular'),
            url(/static/iconfont/MaterialIcons-Regular.woff2) format('woff2'),
            url(/static/iconfont/MaterialIcons-Regular.woff) format('woff'),
            url(/static/iconfont/MaterialIcons-Regular.ttf) format('truetype');
        }

        .material-icons {
            font-family: 'Material Icons';
            font-weight: normal;
            font-style: normal;
            font-size: 24px; /* Preferred icon size */
            display: inline-block;
            line-height: 1;
            text-transform: none;
            letter-spacing: normal;
            word-wrap: normal;
            white-space: nowrap;
            direction: ltr;

            /* Support for all WebKit browsers. */
            -webkit-font-smoothing: antialiased;
            /* Support for Safari and Chrome. */
            text-rendering: optimizeLegibility;

            /* Support for Firefox. */
            -moz-osx-font-smoothing: grayscale;

            /* Support for IE. */
            font-feature-settings: 'liga';
        }
    </style>

    <link rel="stylesheet" href="/static/materialize/css/materialize.css">

    {% block head %}{% endblock head %}
    {% block css %}{% endblock css %}

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    {% block extracss %}{% endblock %}
</head>
<body>
<div class="navbar-fixed">
    <nav>
        <div class="nav-wrapper red">
            <ul class="left">
                <li class="active"><a href="{{ url_for('main.index') }}">Home</a></li>
            </ul>
            <ul id="rightnavbuttons" class="right">
                {% if account %}
                    {% if account.user_level > 300 %}
                        <li><a id="manage-accounts-button" href="{{ url_for('account.manage_accounts') }}">Manage
                            Accounts</a></li>
                    {% endif %}
                    <li><a id="main-logout-button" href="{{ url_for('auth.logout') }}">Logout</a></li>
                {% endif %}
            </ul>
        </div>
    </nav>
</div>

{% block extranav %}
{% endblock %}

<div class="container">

    {% with messages = get_flashed_messages(with_categories=True) %}
        {% if messages %}
            <div class="row">
                <div class="col s12 m6 offset-m3 ">
                    <div class="card z-depth-1">
                        <div class="card-content black-text">
                            <span class="card-title">Messages:</span>
                            <ul>
                                {% for cat, message in messages %}
                                    <li class="{{ cat + "-text" if cat }}">{{ message }}</li>
                                {% endfor %}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        {% endif %}
    {% endwith %}

    {% block body %}This is a template{% endblock body %}
</div>

{% block javascript %}
    <script></script>
{% endblock %}

{% block extrajavascript %}
    <script></script>
{% endblock %}

<!--<script type="text/javascript" src="/static/jquery-3.5.0.min.js"></script> -->
<script>
    document.addEventListener('DOMContentLoaded', function () {
        let elems = document.querySelectorAll('#mobile-menu');
        let instances = M.Sidenav.init(elems, {edge: 'right'});
        let dropdowns = document.querySelectorAll('.dropdown-trigger');
        let dropinst = M.Dropdown.init(dropdowns, {});
        let selects = document.querySelectorAll('select');
        let selinst = M.FormSelect.init(selects, {});
        let dates = document.querySelectorAll('.datepicker');
        let dateinst = M.Datepicker.init(dates, {
            format: 'yyyy-mm-dd'
        });
        let tooltipped = document.querySelectorAll('.tooltipped');
        let tooltippedinst = M.Tooltip.init(tooltipped, {});

        let tabs = document.querySelectorAll(".tabs");
        let tabInst = M.Tabs.init(tabs, {});
    });

    async function postData(url = '', data = {}) {
        // Default options are marked with *
        const response = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'manual', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });
        return response.json(); // parses JSON response into native JavaScript objects
    }
</script>
</body>
</html>