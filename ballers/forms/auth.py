from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import DataRequired

from chance_common.common.forms import CommonFlaskForm


class LoginForm(CommonFlaskForm):
    email = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
