from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import DataRequired, Length, EqualTo, Email

from chance_common.common.forms import CommonFlaskForm


class UpdateAccountForm(CommonFlaskForm):
    email = StringField('Username', validators=[DataRequired(), Email()])
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])

    # address_1 = StringField('Address Line 1')
    # address_2 = StringField('Address Line 2')
    # city = StringField('City')
    # state = StringField('State')
    # county = StringField('County')
    # country = StringField('Country')
    # zip_code = StringField('Zip Code')
    #
    # phone = StringField('Phone')


class CreateAccountForm(CommonFlaskForm):
    email = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', [
        Length(min=2),
        DataRequired(),
        EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Confirm password')
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    is_admin = BooleanField('Is Admin?')
