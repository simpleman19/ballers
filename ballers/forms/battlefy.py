from wtforms import StringField
from wtforms.validators import DataRequired

from chance_common.common.forms import CommonFlaskForm


class TeamOverrideForm(CommonFlaskForm):
    override_name = StringField('Override Team Name', validators=[])


class AddOrgForm(CommonFlaskForm):
    slug = StringField('Org Slug', validators=[DataRequired()])


class ManuallyAddTournamentForm(CommonFlaskForm):
    guid = StringField('Tournament Guid', validators=[DataRequired()])
