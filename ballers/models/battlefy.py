from . import db, BaseModel
from datetime import datetime

team_to_participant = db.Table('btlfy_team_to_participant',
                               db.Column('team_id', db.Integer, db.ForeignKey('btlfy_team.id'), primary_key=True),
                               db.Column('participant_id', db.Integer, db.ForeignKey('btlfy_participant.id'),
                                         primary_key=True)
                               )


class Organization(BaseModel):
    __tablename__ = 'btlfy_organization'
    btlfy_id = db.Column(db.String(128), nullable=False, unique=True)
    name = db.Column(db.String(250))
    slug = db.Column(db.String(250), unique=True)
    excluded_fields = []

    def __init__(self, json_dict=None):
        super().__init__()
        if json_dict:
            self.from_json(json_dict)

    def from_json(self, json_dict):
        self.btlfy_id = json_dict.get('_id')
        self.slug = json_dict.get('slug')
        self.name = json_dict.get('name')


class Tournament(BaseModel):
    __tablename__ = 'btlfy_tournament'
    btlfy_id = db.Column(db.String(128), nullable=False, unique=True)
    start_time = db.Column(db.DateTime)
    name = db.Column(db.String(300))
    region = db.Column(db.String(300))
    slug = db.Column(db.String(300))
    game_name = db.Column(db.String(250))
    status = db.Column(db.String(100))
    team_count = db.Column(db.Integer)
    org_id = db.Column(db.Integer, db.ForeignKey('btlfy_organization.id'))
    org = db.relationship("Organization")
    upcoming = db.Column(db.Boolean, nullable=False, default=False)
    excluded_fields = []

    def __init__(self, org, json_dict=None):
        super().__init__()
        self.org = org
        if json_dict:
            self.from_json(json_dict)

    def from_json(self, json_dict):
        self.btlfy_id = json_dict.get('_id')
        date_time_obj = datetime.strptime(json_dict.get('startTime')[:-5], '%Y-%m-%dT%H:%M:%S')
        self.start_time = date_time_obj
        self.name = json_dict.get('name')
        self.region = json_dict.get('region')
        self.slug = json_dict.get('slug')
        name = json_dict.get('game', {}).get('name')
        if not name:
            name = json_dict.get('gameName')
        if name:
            self.game_name = name
        status = json_dict.get('status')
        if status:
            self.status = status
        count = json_dict.get('teamsCount')
        if count:
            self.team_count = count


class Stage(BaseModel):
    __tablename__ = 'btlfy_stage'
    btlfy_id = db.Column(db.String(128), nullable=False, unique=True)
    tourn_id = db.Column(db.Integer, db.ForeignKey('btlfy_tournament.id'))
    tournament = db.relationship("Tournament")
    excluded_fields = []

    def __init__(self, tournament, json_dict=None):
        super().__init__()
        self.tournament = tournament
        if json_dict:
            self.from_json(json_dict)

    def from_json(self, json_dict):
        self.btlfy_id = json_dict.get('_id')


class Match(BaseModel):
    __tablename__ = 'btlfy_match'
    btlfy_id = db.Column(db.String(128), nullable=False, unique=True)
    stage_id = db.Column(db.Integer, db.ForeignKey('btlfy_stage.id'))
    stage = db.relationship("Stage")
    tourn_id = db.Column(db.Integer, db.ForeignKey('btlfy_tournament.id'))
    tournament = db.relationship("Tournament")
    match_number = db.Column(db.Integer)
    round_number = db.Column(db.Integer)
    is_complete = db.Column(db.Boolean, default=False)
    match_type = db.Column(db.String(40), default='')
    top_team_btlfy_id = db.Column(db.String(128), nullable=True)
    top_team_id = db.Column(db.Integer, db.ForeignKey('btlfy_team.id'))
    top_team = db.relationship("Team", foreign_keys=[top_team_id])
    top_is_winner = db.Column(db.Boolean, default=False)
    bottom_team_btlfy_id = db.Column(db.String(128), nullable=True)
    bottom_team_id = db.Column(db.Integer, db.ForeignKey('btlfy_team.id'))
    bottom_team = db.relationship("Team", foreign_keys=[bottom_team_id])
    bottom_is_winner = db.Column(db.Boolean, default=False)
    excluded_fields = []

    def __init__(self, tournament, stage, json_dict=None):
        super().__init__()
        self.tournament = tournament
        self.stage = stage
        if json_dict:
            self.from_json(json_dict)

    def to_dict(self):
        data = super().to_dict()
        if self.top_team:
            data['top_team'] = self.top_team.to_dict()
        if self.bottom_team:
            data['bottom_team'] = self.bottom_team.to_dict()
        return data

    def from_json(self, json_dict):
        self.btlfy_id = json_dict.get('_id')
        self.match_number = json_dict.get('matchNumber')
        self.round_number = json_dict.get('roundNumber')
        self.is_complete = json_dict.get('isComplete', False)
        self.top_team_btlfy_id = json_dict.get('top', {}).get('teamID')
        self.top_is_winner = json_dict.get('top', {}).get('winner', False)
        self.bottom_team_btlfy_id = json_dict.get('bottom', {}).get('teamID')
        self.bottom_is_winner = json_dict.get('bottom', {}).get('winner', False)
        self.match_type = json_dict.get('matchType')


class Team(BaseModel):
    __tablename__ = 'btlfy_team'
    btlfy_id = db.Column(db.String(128), nullable=False, unique=True)
    name = db.Column(db.String(300))
    btlfy_tourn_id = db.Column(db.String(128), nullable=False)
    btlfy_owner_id = db.Column(db.String(128), nullable=False)
    btlfy_captain_id = db.Column(db.String(128), nullable=True)
    tourn_id = db.Column(db.Integer, db.ForeignKey('btlfy_tournament.id'))
    tournament = db.relationship("Tournament")
    players = db.relationship('Participant', secondary=team_to_participant,
                              backref=db.backref('teams', lazy=True))
    override_name = db.Column(db.String(300), nullable=True, default='')
    excluded_fields = []

    def __init__(self, tournament, json_dict=None):
        super().__init__()
        self.tournament = tournament
        if json_dict:
            self.from_json(json_dict)

    def from_json(self, json_dict):
        self.btlfy_id = json_dict.get('_id')
        self.name = json_dict.get('name')
        self.btlfy_tourn_id = json_dict.get('tournamentID')
        self.btlfy_owner_id = json_dict.get('ownerID')
        self.btlfy_captain_id = json_dict.get('captainID')

    def to_dict(self):
        data = super().to_dict()
        if self.players:
            data['players'] = [p.to_dict() for p in self.players]
        return data


class Player(BaseModel):
    __tablename__ = 'btlfy_player'
    btlfy_id = db.Column(db.String(128), nullable=False, unique=True)
    name = db.Column(db.String(300))
    slug = db.Column(db.String(300))
    username = db.Column(db.String(300))
    excluded_fields = []

    def __init__(self, json_dict=None):
        super().__init__()
        if json_dict:
            self.from_json(json_dict)

    def from_json(self, json_dict):
        self.btlfy_id = json_dict.get('_id')
        self.name = json_dict.get('name')
        self.slug = json_dict.get('userSlug')
        self.username = json_dict.get('username')


class Participant(BaseModel):
    __tablename__ = 'btlfy_participant'
    btlfy_id = db.Column(db.String(128), nullable=False, unique=True)
    in_game_name = db.Column(db.String(300))
    btlfy_user_id = db.Column(db.String(128))
    btlfy_owner_id = db.Column(db.String(128))
    btlfy_tournament_id = db.Column(db.String(128))
    tourn_id = db.Column(db.Integer, db.ForeignKey('btlfy_tournament.id'))
    tournament = db.relationship("Tournament")
    excluded_fields = []

    def __init__(self, tournament, json_dict=None):
        super().__init__()
        self.tournament = tournament
        if json_dict:
            self.from_json(json_dict)

    def from_json(self, json_dict):
        self.btlfy_id = json_dict.get('_id')
        self.in_game_name = json_dict.get('inGameName')
        self.btlfy_user_id = json_dict.get('userID')
        self.btlfy_owner_id = json_dict.get('ownerID')
        self.btlfy_tournament_id = json_dict.get('tournamentID')
