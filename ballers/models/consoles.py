from datetime import datetime

from . import db, BaseModel


class Console(BaseModel):
    __tablename__ = 'consoles'
    name = db.Column(db.String(50))
    team1_text = db.Column(db.String(100))
    team2_text = db.Column(db.String(100))
    team3_text = db.Column(db.String(100))
    team4_text = db.Column(db.String(100))
    player1_text = db.Column(db.String(400))
    player2_text = db.Column(db.String(400))
    player3_text = db.Column(db.String(400))
    player4_text = db.Column(db.String(400))
    map_1 = db.Column(db.Integer, default=0)
    map_2 = db.Column(db.Integer, default=0)
    round_number = db.Column(db.String(100))
    match_number = db.Column(db.String(100))
    updated_timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def clear(self):
        self.team1_text = ''
        self.team2_text = ''
        self.team3_text = ''
        self.team4_text = ''
        self.player1_text = ''
        self.player2_text = ''
        self.player3_text = ''
        self.player4_text = ''
        self.map_1 = 0
        self.map_2 = 0
        self.round_number = ''
        self.match_number = ''

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'team1_text': self.team1_text,
            'team2_text': self.team2_text,
            'team3_text': self.team3_text,
            'team4_text': self.team4_text,
            'player1_text': self.player1_text,
            'player2_text': self.player2_text,
            'player3_text': self.player3_text,
            'player4_text': self.player4_text,
            'map_1': self.map_1,
            'map_2': self.map_2,
            'round_number': self.round_number,
            'match_number': self.match_number,
            'updated_timestamp': self.updated_timestamp,
        }

    def from_dict(self, source_dict: dict):
        fields = filter(lambda a: not a.startswith('__') and a, source_dict.keys())
        for f in fields:
            val = source_dict.get(f, None)
            if val:
                setattr(self, f, val)
        return self
