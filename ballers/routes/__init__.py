import os
import re
from datetime import datetime
from typing import List

from flask import Blueprint, render_template, redirect, url_for, request, flash, current_app as app, g, \
    send_from_directory, abort, send_file

from chance_common.common.custom_logging import get_logger
from chance_common.common.api_response import ApiResult, ApiException
from chance_common.common.auth import token_auth
from chance_common.common.flask_decorators import templated_or_api, api, templated
from chance_common.common.forms import hydrate
from ballers.models.battlefy import Organization, Tournament, Match, Team, Stage, Participant
from ballers.models.consoles import Console
from ballers.tasks import refresh_tournament, get_organization, refresh_organization
from chance_common.common.utils import require_json
from ballers.models import db
from ballers.forms.battlefy import TeamOverrideForm, AddOrgForm, ManuallyAddTournamentForm

main = Blueprint('main', __name__, url_prefix='')

root_path = os.path.dirname(os.path.abspath(__file__))

logger = get_logger(__name__)


@main.route('/static/<path:path>')
def send_js(path):
    return send_from_directory('static', path)


@templated_or_api(main, '/', 'index.jn2', methods=['GET'])
@token_auth.login_required
def index(**kwargs):
    orgs = Organization.query.all()
    return dict(orgs=orgs)


@templated_or_api(main, '/org', 'add_org.jn2', methods=['GET', 'POST'])
@token_auth.login_required
def add_org(**kwargs):
    form = AddOrgForm()
    if form.validate_on_submit():
        slug = form.slug.data
        org = get_organization(slug)
        if org:
            return redirect(url_for('main.show_org', guid=org.guid))
    return dict(form=form)


@templated_or_api(main, '/org/<string:guid>/add_tourn', 'add_tourn.jn2', methods=['GET', 'POST'])
@token_auth.login_required
def add_tourn(guid, **kwargs):
    org = Organization.query.filter_by(guid=guid).one_or_none()
    form = ManuallyAddTournamentForm()
    if form.validate_on_submit():
        tourn_guid = form.guid.data
        org = refresh_tournament(org, tourn_guid)
        if org:
            return redirect(url_for('main.show_org', guid=org.guid))
    return dict(form=form, guid=guid)


@main.route('/org/<string:guid>/refresh')
@token_auth.login_required
def refresh_org(guid):
    org = Organization.query.filter_by(guid=guid).one_or_none()
    if org:
        refresh_organization(org.guid)
        flash("Org updated")
    else:
        flash("Error finding org")
    return redirect(url_for('main.show_org', guid=org.guid))


@templated_or_api(main, '/org/<string:guid>', 'org.jn2', methods=['GET'])
@token_auth.login_required
def show_org(guid, **kwargs):
    org = Organization.query.filter_by(guid=guid).one_or_none()
    tourns = Tournament.query.filter_by(org_id=org.id, upcoming=True).order_by(Tournament.start_time.asc()).all()
    tourns_past = Tournament.query.filter_by(org_id=org.id, upcoming=False).order_by(Tournament.start_time.desc()).all()
    return dict(org=org, tourns=tourns, tourns_past=tourns_past)


def build_tourn_dict(guid, tourn_guid):
    show_all = request.args.get('all') == 'true'
    org = Organization.query.filter_by(guid=guid).one_or_none()
    tourn = Tournament.query.filter_by(org_id=org.id, guid=tourn_guid).one_or_none()
    if show_all:
        matches_win = Match.query.filter_by(tourn_id=tourn.id, match_type='winner').order_by(Match.match_number).all()
        matches_lose = Match.query.filter_by(tourn_id=tourn.id, match_type='loser').order_by(Match.match_number).all()
    else:
        matches_win = Match.query.filter_by(tourn_id=tourn.id, match_type='winner', is_complete=False).order_by(
            Match.match_number).all()
        matches_lose = Match.query.filter_by(tourn_id=tourn.id, match_type='loser', is_complete=False).order_by(
            Match.match_number).all()
    consoles = Console.query.all()
    return dict(org=org, tourn=tourn, matches_win=matches_win, matches_lose=matches_lose, consoles=consoles,
                show_all=show_all)


@templated(main, '/org/<string:guid>/<string:tourn_guid>/matches', 'tourn.jn2')
@token_auth.login_required
def show_tourn(guid, tourn_guid, **kwargs):
    return build_tourn_dict(guid, tourn_guid)


@templated_or_api(main, '/org/<string:guid>/<string:tourn_guid>/live', 'live.jn2')
@token_auth.login_required
def show_live(guid, tourn_guid, **kwargs):
    live_dict = build_tourn_dict(guid, tourn_guid)
    return live_dict


@templated_or_api(main, '/manual_tourn', 'manual.jn2')
@token_auth.login_required
def manual_tourn(**kwargs):
    org = Organization.query.filter_by(slug='manual').one_or_none()
    if not org:
        org = Organization()
        org.name = "Manual Tourn"
        org.slug = "manual"
        org.btlfy_id = 'manual'
        db.session.add(org)
        db.session.commit()
    tourn = Tournament.query.filter_by(org_id=org.id, slug='manual').one_or_none()
    if not tourn:
        tourn = Tournament(org)
        tourn.btlfy_id = 'manual'
        tourn.start_time = datetime.now()
        tourn.name = 'Manual Tournament'
        tourn.region = 'N/A'
        tourn.slug = 'manual'
        tourn.game_name = 'manual'
        tourn.status = 'In Progress'
        tourn.upcoming = False
        tourn.team_count = 0
        db.session.add(tourn)
        db.session.commit()

    stage = Stage.query.filter_by(tourn_id=tourn.id).one_or_none()
    if not stage:
        stage = Stage(tourn)
        stage.btlfy_id = 'manual'
        db.session.add(stage)
        db.session.commit()
    matches = Match.query.filter_by(tourn_id=tourn.id).order_by(Match.match_number).all()
    teams = Team.query.filter_by(tourn_id=tourn.id).order_by(Team.name).all()
    return {
        'org': org,
        'tourn': tourn,
        'stage': stage,
        'matches': matches,
        'teams': teams,
    }


@api(main, '/manual/team/add', methods=['POST'])
@token_auth.login_required
@require_json()
def add_team_manual(data, **kwargs):
    org = Organization.query.filter_by(slug='manual').one_or_none()
    tourn = Tournament.query.filter_by(org_id=org.id, slug='manual').one_or_none()
    for p in data:
        if p.get('team_name', '').strip():
            team = Team(tourn)
            team.btlfy_id = team.guid
            team.name = p.get('team_name')
            team.btlfy_tourn_id = 'manual'
            participant = Participant(tourn)
            participant.btlfy_id = participant.guid
            participant.btlfy_owner_id = participant.guid
            participant.btlfy_user_id = participant.guid
            participant.btlfy_tournament_id = 'manual'
            participant.in_game_name = p.get('team_players')
            db.session.add(participant)
            team.players.append(participant)
            team.btlfy_owner_id = participant.guid
            team.btlfy_captain_id = participant.guid
            db.session.add(team)
    db.session.commit()
    return {'success': True}


@api(main, '/manual/match/add', methods=['POST'])
@token_auth.login_required
@require_json()
def add_match_manual(data, **kwargs):
    org = Organization.query.filter_by(slug='manual').one_or_none()
    tourn = Tournament.query.filter_by(org_id=org.id, slug='manual').one_or_none()
    stage = Stage.query.filter_by(tourn_id=tourn.id).one_or_none()
    for m in data:
        team1_id = m.get('team1')
        team2_id = m.get('team2')
        if team1_id and team2_id:
            team1 = Team.query.get(team1_id)
            team2 = Team.query.get(team2_id)
            if team1 and team2:
                match = Match(tourn, stage)
                match.btlfy_id = match.guid
                match.match_number = m.get('match', 0)
                match.round_number = m.get('round', 0)
                match.is_complete = False
                match.match_type = m.get('type', 'winner')
                match.top_team_btlfy_id = team1.guid
                match.top_team = team1
                match.bottom_team_btlfy_id = team2.guid
                match.bottom_team = team2
                db.session.add(match)
    db.session.commit()
    return {'success': True}


@main.route('/org/<string:guid>/<string:tourn_guid>/refresh')
@token_auth.login_required
def refresh_tourn(guid, tourn_guid):
    org = Organization.query.filter_by(guid=guid).one_or_none()
    tourn = Tournament.query.filter_by(org_id=org.id, guid=tourn_guid).one_or_none()
    if tourn:
        refresh_tournament(org, tourn.guid)
        flash("Tournament updated")
    else:
        flash("Error finding tournament")
    return redirect(url_for('main.show_tourn', guid=org.guid, tourn_guid=tourn.guid))


@api(main, '/org/<string:guid>/<string:tourn_guid>/refresh', methods=['POST'])
def refresh_tourn_api(guid, tourn_guid, **kwargs):
    org = Organization.query.filter_by(guid=guid).one_or_none()
    tourn = Tournament.query.filter_by(org_id=org.id, guid=tourn_guid).one_or_none()
    if tourn:
        refresh_tournament(org, tourn.guid)
        return {'success': True}
    else:
        raise ApiException('Could not find tournament or org', 404)


@api(main, '/console/<int:con_id>', methods=['POST'])
@token_auth.login_required
@require_json(['match_guid'])
def attach_text_to_console(con_id, data, **kwargs):
    con: Console = Console.query.get(con_id)
    m: Match = Match.query.filter_by(guid=data.get('match_guid')).one_or_none()
    if con and m:
        con.clear()
        if m.top_team:
            if m.top_team.override_name:
                con.team1_text = m.top_team.override_name
            else:
                con.team1_text = m.top_team.name
            p_names = ''
            for p in m.top_team.players:
                p_names += f"{p.in_game_name} : "
            con.player1_text = p_names
        if m.bottom_team:
            if m.bottom_team.override_name:
                con.team2_text = m.bottom_team.override_name
            else:
                con.team2_text = m.bottom_team.name
            p_names = ''
            for p in m.bottom_team.players:
                p_names += f"{p.in_game_name} : "
            con.player2_text = p_names
        match_type = 'L'
        if m.match_type == 'winner':
            match_type = 'W'
        con.round_number = f"{match_type} R: {m.round_number} : M: {m.match_number}"
    else:
        raise ApiException('Could not find console or match', 404)
    db.session.commit()
    return {'success': True}


@api(main, '/console/<int:con_id>/map/<int:map_num>/<string:mod_type>', methods=['POST'])
@token_auth.login_required
def change_map_count(con_id, map_num, mod_type, **kwargs):
    con: Console = Console.query.get(con_id)
    if con:
        if mod_type == 'clear':
            con.map_1 = 0
            con.map_2 = 0
        elif map_num == 1:
            if mod_type == 'inc':
                con.map_1 += 1
            else:
                con.map_1 -= 1
        elif map_num == 2:
            if mod_type == 'inc':
                con.map_2 += 1
            else:
                con.map_2 -= 1
        db.session.commit()
    else:
        raise ApiException('Could not find console or match', 404)
    return {'success': True}


@api(main, '/console/<int:con_id>/swap', methods=['POST'])
@token_auth.login_required
def swap_console_text(con_id, **kwargs):
    con: Console = Console.query.get(con_id)
    if con:
        swap_console_info(con)
        db.session.commit()
    else:
        raise ApiException('Could not find console or match', 404)
    return {'success': True}


@api(main, '/console/<int:con_id>/clear', methods=['POST'])
@token_auth.login_required
def clear_console_text(con_id, **kwargs):
    con: Console = Console.query.get(con_id)
    if con:
        con.clear()
        db.session.commit()
    else:
        raise ApiException('Could not find console or match', 404)
    return {'success': True}


@main.route('/org/<string:guid>/<string:tourn_guid>/teams')
@token_auth.login_required
def show_teams(guid, tourn_guid):
    org = Organization.query.filter_by(guid=guid).one_or_none()
    tourn = Tournament.query.filter_by(org_id=org.id, guid=tourn_guid).one_or_none()
    teams = Team.query.filter_by(tourn_id=tourn.id).order_by(Team.name).all()
    return render_template('teams.jn2', org=org, tourn=tourn, teams=teams)


@main.route('/org/<string:guid>/<string:tourn_guid>/team/<string:team_guid>', methods=['GET', 'POST'])
@token_auth.login_required
def edit_team(guid, tourn_guid, team_guid):
    org = Organization.query.filter_by(guid=guid).one_or_none()
    tourn = Tournament.query.filter_by(org_id=org.id, guid=tourn_guid).one_or_none()
    team = Team.query.filter_by(tourn_id=tourn.id, guid=team_guid).order_by(Team.name).one_or_none()
    form = TeamOverrideForm(obj=team)
    if form.validate_on_submit():
        override_name = form.override_name.data
        team.override_name = override_name.strip()
        db.session.commit()
        return redirect(url_for('main.show_teams', guid=guid, tourn_guid=tourn_guid))
    return render_template('edit_team.jn2', form=form, org=org, tourn=tourn, team=team)


@api(main, '/console/<int:con_id>', methods=['GET'])
def get_console(con_id, **kwargs):
    con: Console = Console.query.get(con_id)
    return dict(console=con)


@api(main, '/consoles', methods=['GET'])
def get_all_consoles(**kwargs):
    logger.debug('Getting all console information')
    cons: List[Console] = Console.query.order_by(Console.name).all()
    return cons


@main.route('/health_check')
def health_check():
    return ApiResult(value={'status': 'healthy'}, status=208).to_response()


@main.errorhandler(ApiException)
def handle_api_exception(error: ApiException):
    return error.to_result().to_response()


def swap_console_info(c1: Console):
    c1d = c1.to_dict()
    c1.team1_text = c1d['team2_text']
    c1.team2_text = c1d['team1_text']
    c1.player1_text = c1d['player2_text']
    c1.player2_text = c1d['player1_text']
    c1.map_1 = c1d['map_2']
    c1.map_2 = c1d['map_1']
    return c1
