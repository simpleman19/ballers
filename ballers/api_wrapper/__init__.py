from flask import current_app as app
import os
from chance_common.common.utils import get_from_config_or_environ

url_cache = {}


def get_btlfy_search_url():
    return 'https://search.battlefy.com/'


def get_btlfy_cloudfront_url():
    return 'https://dtmwra1jsgyb0.cloudfront.net/'
