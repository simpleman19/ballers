from datetime import datetime

from chance_common.common.api_wrapper import get_url_and_return_json
from chance_common.common.custom_logging import get_logger

from ballers.api_wrapper import get_btlfy_search_url, get_btlfy_cloudfront_url

logger = get_logger(__name__)

search_tournaments = 'tournament/organization/'


def get_organization(slug: str):
    logger.debug("Getting org id for slug: %s", slug)
    url = get_btlfy_cloudfront_url() + 'organizations?slug=' + slug
    ret_json = get_url_and_return_json(url)
    if ret_json and ret_json[0]:
        return ret_json[0]
    return None


def get_upcoming_tournaments(org_id):
    logger.debug("Getting upcoming tournaments for: %s", org_id)
    url = get_btlfy_search_url() + search_tournaments + org_id + '/upcoming'
    return get_url_and_return_json(url)


def get_past_tournaments(org_id):
    logger.debug("Getting past tournaments for: %s", org_id)
    url = get_btlfy_search_url() + search_tournaments + org_id + '/past?page=1&size=10'
    return get_url_and_return_json(url)


def get_full_tournament_info(tourn_id):
    logger.debug("Getting tournament info for: %s", tourn_id)
    url = get_btlfy_cloudfront_url() + 'tournaments/' + tourn_id
    return get_url_and_return_json(url)


def get_matches_for_stage(stage_id, updated=None):
    logger.debug("Getting matches for stage: %s", stage_id)
    url = get_btlfy_cloudfront_url() + 'stages/' + stage_id + '/matches'
    if updated:
        url += '?updatedAt[$gt]=2020-08-30T21:49:28.180Z'
    return get_url_and_return_json(url)


def get_participants_for_tournament(tourn_id, updated=None):
    logger.debug("Getting participants for tournament: %s", tourn_id)
    url = get_btlfy_cloudfront_url() + 'tournaments/' + tourn_id + '/participants'
    if updated:
        url += '?updatedAt[$gt]=2020-08-30T21:49:28.180Z'
    return get_url_and_return_json(url)


def get_teams_for_tournament(tourn_id):
    logger.debug("Getting teams for tournament: %s", tourn_id)
    url = get_btlfy_cloudfront_url() + 'tournaments/' + tourn_id + '/teams'
    return get_url_and_return_json(url)


# https://dtmwra1jsgyb0.cloudfront.net/matches/5f4bec0ce425a655aadb86c4?extend%5Btop.team%5D%5Bplayers%5D%5Buser%5D=true&extend%5Btop.team%5D%5BpersistentTeam%5D=true&extend%5Bbottom.team%5D%5Bplayers%5D%5Buser%5D=true&extend%5Bbottom.team%5D%5BpersistentTeam%5D=true&extend%5Bstats%5D=true
# https://dtmwra1jsgyb0.cloudfront.net/matches/5f4bec0ce425a655aadb86c4?extend[top.team][players][user]=true&extend[top.team][persistentTeam]=true&extend[bottom.team][players][user]=true&extend[bottom.team][persistentTeam]=true&extend[stats]=true


if __name__ == '__main__':
    id = get_organization('clickgaming')['_id']
    if id:
        print(get_upcoming_tournaments(id))
        get_participants_for_tournament('5f4bec0ce425a655aadb86c4')
    time = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')
    time = time[:-3] + 'Z'
    print(time)
