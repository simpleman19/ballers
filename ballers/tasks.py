from typing import List

from ballers import celery, db
from flask import current_app as app
from time import mktime
from datetime import datetime
import os

from ballers.models.battlefy import Organization, Tournament, Stage, Match, Player, Team, Participant
from chance_common.common.custom_logging import get_logger

RETRY_TIMES = 5

logger = get_logger(__name__)

file_extensions = None


@celery.task
def process_transaction(transaction_id):
    from ballers import create_app
    app = create_app()
    with app.app_context():
        print('TODO')


def get_organization(slug):
    from ballers.api_wrapper.battlefy import get_organization
    org_dict = get_organization(slug)
    org: Organization = Organization.query.filter_by(slug=slug).one_or_none()
    if not org:
        org = Organization(json_dict=org_dict)
    else:
        org.from_json(json_dict=org_dict)
    db.session.add(org)
    db.session.commit()
    return org


def get_upcoming_tournaments_for_org(org_guid):
    from ballers.api_wrapper.battlefy import get_upcoming_tournaments

    org: Organization = Organization.query.filter_by(guid=org_guid).one_or_none()
    if not org:
        raise Exception()

    tourns = get_upcoming_tournaments(org.btlfy_id)
    if tourns:
        tourns = tourns.get('tournaments')
    for t in tourns:
        btlfy_id = t.get('_id')
        tourn: Tournament = Tournament.query.filter_by(btlfy_id=btlfy_id).one_or_none()
        if not tourn:
            tourn = Tournament(org=org, json_dict=t)
        else:
            tourn.from_json(json_dict=t)
        tourn.upcoming = True
        db.session.add(tourn)
    db.session.commit()


def get_past_tournaments_for_org(org_guid):
    from ballers.api_wrapper.battlefy import get_past_tournaments

    org: Organization = Organization.query.filter_by(guid=org_guid).one_or_none()
    if not org:
        raise Exception()

    tourns = get_past_tournaments(org.btlfy_id)
    if tourns:
        tourns = tourns.get('tournaments')
    for t in tourns:
        btlfy_id = t.get('_id')
        tourn: Tournament = Tournament.query.filter_by(btlfy_id=btlfy_id).one_or_none()
        if not tourn:
            tourn = Tournament(org=org, json_dict=t)
        else:
            tourn.from_json(json_dict=t)
        tourn.upcoming = False
        db.session.add(tourn)
    db.session.commit()


def load_tourn_info(org, tourn_guid):
    from ballers.api_wrapper.battlefy import get_full_tournament_info

    tourn: Tournament = Tournament.query.filter_by(guid=tourn_guid).one_or_none()
    if not tourn:
        tourn = Tournament(org=org)
        tourn.btlfy_id = tourn_guid
    tourn_json = get_full_tournament_info(tourn.btlfy_id)
    tourn.from_json(tourn_json)
    for stageId in tourn_json.get('stageIDs', []):
        stage_dict = {'_id': stageId}
        stage = Stage.query.filter_by(btlfy_id=stageId).one_or_none()
        if not stage:
            stage = Stage(tourn, json_dict=stage_dict)
        stage.from_json(stage_dict)
        db.session.add(stage)

    db.session.commit()


def load_teams_and_participants_for_tournament(tourn_guid):
    from ballers.api_wrapper.battlefy import get_participants_for_tournament, get_teams_for_tournament

    tourn: Tournament = Tournament.query.filter_by(guid=tourn_guid).one_or_none()
    if not tourn:
        return False

    participants = get_participants_for_tournament(tourn.btlfy_id)
    if participants:
        for participant_json in participants:
            t_btlfy_id = participant_json.get('_id')
            p = Participant.query.filter_by(btlfy_id=t_btlfy_id).one_or_none()
            if not p:
                p = Participant(tourn, json_dict=participant_json)
            p.from_json(participant_json)
            db.session.add(p)

    teams = get_teams_for_tournament(tourn.btlfy_id)
    if teams:
        for team_json in teams:
            t_btlfy_id = team_json.get('_id')
            t = Team.query.filter_by(btlfy_id=t_btlfy_id).one_or_none()
            if not t:
                t = Team(tourn, team_json)
            t.from_json(team_json)
            for player_json in team_json.get('players'):
                p_btlfy_id = player_json.get('_id')
                p = Participant.query.filter_by(btlfy_id=p_btlfy_id).one_or_none()
                if not p:
                    logger.warn('Failed to find participant: %s for team: %s', p_btlfy_id, t_btlfy_id)
                    p = Participant(tourn, json_dict=player_json)
                    db.session.add(p)
                t.players.append(p)
            db.session.add(t)
    db.session.commit()


def load_matches_for_tourn(tourn_guid):
    from ballers.api_wrapper.battlefy import get_matches_for_stage

    tourn: Tournament = Tournament.query.filter_by(guid=tourn_guid).one_or_none()
    if not tourn:
        return False
    stages: List[Stage] = Stage.query.filter_by(tourn_id=tourn.id).all()
    if not stages:
        return False

    for stage in stages:
        matches_json = get_matches_for_stage(stage.btlfy_id)
        if not matches_json:
            matches_json = []
        for match_json in matches_json:
            match = Match.query.filter_by(btlfy_id=match_json.get('_id')).one_or_none()
            if not match:
                match = Match(tourn, stage, json_dict=match_json)
            match.from_json(match_json)

            if match.top_team_btlfy_id:
                team = Team.query.filter_by(btlfy_id=match.top_team_btlfy_id).one_or_none()
                if not team:
                    logger.warn('Failed to find team for team: %s match: %s', match.top_team_btlfy_id, match.btlfy_id)
                    team_json = match_json.get('top', {}).get('team')
                    if team_json:
                        team = Team(tourn, json_dict=team_json)
                        db.session.add(team)
                    else:
                        logger.error("Failed to create team from match json...")
                if team:
                    match.top_team = team
            if match.bottom_team_btlfy_id:
                team = Team.query.filter_by(btlfy_id=match.bottom_team_btlfy_id).one_or_none()
                if not team:
                    logger.warn('Failed to find team for team: %s match: %s', match.bottom_team_btlfy_id,
                                match.btlfy_id)
                    team_json = match_json.get('bottom', {}).get('team')
                    if team_json:
                        team = Team(tourn, json_dict=team_json)
                        db.session.add(team)
                    else:
                        logger.error("Failed to create team from match json...")
                if team:
                    match.bottom_team = team
            db.session.add(match)
            # Clear old matches
            old_matches = Match.query.filter_by(match_type=match.match_type, round_number=match.round_number,
                                                match_number=match.match_number, stage_id=stage.id).all()
            for m in old_matches:
                if m.guid != match.guid:
                    db.session.delete(m)

    db.session.commit()


def refresh_tournament(org, tourn_guid):
    load_tourn_info(org, tourn_guid)
    load_teams_and_participants_for_tournament(tourn_guid)
    load_matches_for_tourn(tourn_guid)


def refresh_tournaments(org_guid):
    get_upcoming_tournaments_for_org(org_guid)
    get_past_tournaments_for_org(org_guid)


def refresh_organization(org_guid):
    org: Organization = Organization.query.filter_by(guid=org_guid).one_or_none()
    get_organization(org.slug)
    refresh_tournaments(org.guid)


if __name__ == '__main__':
    from ballers import create_app

    app = create_app()
    with app.app_context():
        print("Running task: ")
        o = get_organization('clickgaming')
        get_upcoming_tournaments_for_org(o.guid)
        get_past_tournaments_for_org(o.guid)
        tourns: List[Tournament] = Tournament.query.filter_by(org_id=o.id).all()
        for t in tourns:
            refresh_tournament(o, tourn_guid=t.guid)
