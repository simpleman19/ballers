#!/bin/bash

docker build . -t ballers:latest
docker image tag ballers:latest 10.0.0.22:5000/ballers:latest
docker image push 10.0.0.22:5000/ballers:latest